$(document).ready(function($) {
  /*--------------------------------------------------
   common
  --------------------------------------------------*/
  scrollingPage.addRef();
  scrollingPage.clickChangePage();
  scrollingPage.clickNextPage();
  scrollingPage.processKeyWheel();
  scrollingPage.checkClassCurNAV();
  scrollingPage.checkClassCurSec();
  scrollingPage.showbook();
  mainmenu();
});

/*--------------------------------------------------
 all function
--------------------------------------------------*/
function mainmenu(){
  $("#nav ul ").css({display: "none"});
  $("#nav li").hover(function(){
    	$(this).find('ul:first').css({visibility: "visible",display: "none"}).fadeIn(400);
    	$(this).has( "ul" ).addClass('active-nav');
    },function(){
    	$(this).find('ul:first').css({visibility: "hidden"});
    	$(this).has( "ul" ).removeClass('active-nav');
  });
}
var scrollingPage = (function(){
	var containerObj = $('#container'),
	    stopsObj = $('.stops'),
	    sectionsEl = $('.sections'),
	    counter = 0,
	    flag = true;
	$(sectionsEl).first().fadeIn().addClass('current-sec');
	function addRef(){
		$(sectionsEl).each(function() {
			counter++;
			$(this).attr('ref', counter);
		});
	}
	function checkClassCurNAV() {
		var selCu= $('.current',stopsObj);
		var secDT = selCu.attr('data-FT');
		return secDT;
	}
	function checkClassCurSec() {
		var indSec= $('.current-sec').index() + 1;
		return indSec;
	}
	
	function clickChangePage(){
		$('li', stopsObj).click(function(event){
			if (flag) {
				var secDT = checkClassCurNAV();
				var insecID = checkClassCurSec();
				$('.current', stopsObj).removeClass('current');
				$(this).addClass('current');
				var $selected = $('.current',stopsObj);
				var sectionID = Number($selected.attr('ref'));
				var nextID = sectionID;
				checkCurrent(nextID, secDT, insecID);
				flag = false;
			} else {
				return false;
			}
			return false;
		});
	}

	function clickNextPage() {
		$('.down-arrow').bind('click', function(event){
		  	if (flag) {
		  		var insecID = checkClassCurSec();
		  		var secDT = checkClassCurNAV();
				var $selected = $('.current',stopsObj);
				var sectionID = Number($selected.attr('ref'));
				var nextID = sectionID + 1;
				checkCurrent(nextID, secDT, insecID);
				flag = false;
			} else {
				return false;
			}
			return false;
		});
	}
	function processKeyWheel(){
		$(document).on("keyup", function(e) {
			var secDT = checkClassCurNAV();
			var insecID = checkClassCurSec();
			var code = e.which;
			if (code == 40) {
				//down pressed
				if (flag) {
					var $selected = $('.current',stopsObj);
					var sectionID = Number($selected.attr('ref'));
					var nextID = sectionID + 1;
					checkCurrent(nextID, secDT, insecID);
					if (nextID >= 5){
						flag = true;
					}else {
						flag = false;
					}
				} else {
					return false;
				}
			} else if (code == 38) {
				//up pressed
				if (flag) {
					var $selected = $('.current',stopsObj);
					var sectionID = Number($selected.attr('ref'));
					var nextID = sectionID - 1;
					if(nextID >= 1){
						checkCurrent(nextID, secDT, insecID);
					}else {
						return false;
					}
					flag = false;
				} else {
					return false;
				}
				
			}
		});
		$(document).on('mousewheel', function(event) {
			var secDT = checkClassCurNAV();
			var insecID = checkClassCurSec();
			var delta = parseInt(event.originalEvent.wheelDelta || -event.originalEvent.detail);
			if(delta > 0) {
				// wheel up
				if (flag) {
					var $selected = $('.current',stopsObj);
					var sectionID = Number($selected.attr('ref'));
					var nextID = sectionID - 1;
					if(nextID >= 1){
						checkCurrent(nextID, secDT, insecID);
					}else {
						return false;
					}
					flag = false;
				} else {
					return false;
				}				
			} else {
				// wheel down
				if (flag) {
					var $selected = $('.current',stopsObj);
					var sectionID = Number($selected.attr('ref'));
					var nextID = sectionID + 1;
					checkCurrent(nextID, secDT, insecID);
					if (nextID >= 5){
						flag = true;
					}else {
						flag = false;
					}
				} else {
					return false;
				}				
			}
		});
	}
	
	function checkCurrent(nextID, secDT, insecID){
		if(nextID <= 5){
			if(nextID == 5){
				$('.down-arrow').fadeOut('slow');
			}else {
				$('.down-arrow').fadeIn('slow');
			}

			//remove class
			$('.current', stopsObj).removeClass('current');
			$('.sections').removeClass('fadeInDown');
			$('.sections').removeClass('fadeInUp');
			$('.sections').removeClass('current-sec');
			$('.removeTick').removeClass("removeTick");

			//add class
			$('li[ref=' + nextID +']', stopsObj).addClass("current");
			var scrollChange = $('.sections[ref=' + nextID +']', containerObj);
			var classinse = $('.sections[ref=' + insecID +']', containerObj);
			
			$(scrollChange).addClass('fadeInDown').fadeIn(1000);
			$(classinse).addClass('fadeInUp').fadeOut(3000);
			$(scrollChange).addClass('current-sec');

			/*var delay = setTimeout(function(){
		        $(scrollChange).switchClass( "fadeInDown", "current-sec", "easeInOutQuad" );
		     },4000)*/



			//process time line
			var abk = nextID -1;
			$(stopsObj).animate({
				top: abk*74,
			}, 3000);

			var DFT = $('li[ref=' + nextID +']', stopsObj).attr('data-FT');	
			var daFT = $('.altimeter span');
			if(DFT){
				$('.altimeter em').stop().animate({
					 opacity: 1,
				}, 3000);
				$({someValue: secDT}).animate({someValue: DFT}, {
				  duration: 3000,
				  easing:'swing', // can be anything
				  step: function() { // called on every step
				      // Update the element's text with rounded-up value:
				      $(daFT).text(commaNumber(Math.round(this.someValue)));
				  },
				  complete:function(){
				      $(daFT).text(commaNumber(Math.round(this.someValue)));
				      $('.current', stopsObj).addClass("removeTick");
				      flag = true;
				  }
				});
			}
			var hideFT = $('li[ref=' + nextID +']', stopsObj).attr('ref');
			if(hideFT == 5){
				$('.altimeter em').stop().animate({
					opacity: 0
				}, 3000, function() {
					flag = true;
					$('.current', stopsObj).addClass("removeTick");
				});
			}
		}else {
			return false;
		}
		
	}
	function commaNumber(val){
		while (/(\d+)(\d{3})/.test(val.toString())){
		  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		}
		return val;
	}
	function showbook(){
		var flagBo = true;
		$('.btn-toggle').click(function(){
			if(flagBo){
				$(this).parent('.book-flight').addClass("change-bo");
				$('.show-book').animate({
					left: 0
				}, 1000, function() {
					flagBo = false;
					flag = false;
				});
				$('.btn-toggle').animate({
					left: "1135px"
				}, 1000);
			}else {
				$('.show-book').animate({
					left: "-1135px"
				}, 1000, function() {
					$(this).parent('.book-flight').removeClass("change-bo");
					flagBo = true;
					flag = true;
				});
				$('.btn-toggle').animate({
					left: 0
				}, 1000);
			}
		});
	}
	return {
		addRef:addRef,
		clickChangePage:clickChangePage,
		clickNextPage:clickNextPage,
		processKeyWheel:processKeyWheel,
		checkCurrent:checkCurrent,
		commaNumber:commaNumber,
		checkClassCurNAV:checkClassCurNAV,
		checkClassCurSec:checkClassCurSec,
		showbook:showbook
	};
})();
