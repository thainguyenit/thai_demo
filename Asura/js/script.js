/*
	Company:	Puropela.com
	Dev: 		thainguyenit@gmail.com
*/
$(document).ready(function(){
	
 	 //Load page effect
	$(function(){
		$('#container').hide();
	    $('#container').fadeIn(2000);	
	});
	
/*--------------------------------------------------
  Click NAV
--------------------------------------------------*/
	var tabObj = $('ul.list-nav');
	var height = $('li', tabObj).outerHeight();
	var elementsHeight = {};
	var counter = 0;
	var containerObj = $('#conDes');
	var flag = true;
	
	
	$('.sections').each(function() {
		counter++;
		$(this).attr('ref', counter);
		elementsHeight['section-' + counter] = $(this).outerHeight(true);
	});
	console.debug(elementsHeight);
	// Click NAV
	$('a', tabObj).click(function(){
		flag = false;
		$('a.current', tabObj).removeClass('current');
		
		var sectionID = $(this).attr('rel');
		var scrollChange = $('.sections[ref=' + sectionID +']', containerObj).offset().top - 64 - height * (sectionID - 1);
		$('html, body').animate({scrollTop: scrollChange}, function() {setTimeout(function() {flag = true;}, 0);});
		$(this).addClass('current');
		
	    return false;
	});
/*--------------------------------------------------
  pagetop fade in
--------------------------------------------------*/
	
	$(".top_link").hide();
	$(window).bind('scroll', function(){
		if($(window).scrollTop() > 200){
			$(".top_link").slideDown("normal");
		}
		else{
			$(".top_link").slideUp("normal");
		}
		
	})		
/*--------------------------------------------------
  pagetop fixed
--------------------------------------------------*/
	function pageFixed() {
		var heightFix = $('body').height() - $(window).scrollTop() - $(window).height();
		if (heightFix < 220) {
			$('.wrapLink').removeClass('sticky');
		}
		else{
			$('.wrapLink').addClass('sticky');	
		}
	}
	//Click on Top page
	$('.top_link').click(function(){
		$('body,html').animate({
			scrollTop: 0 /* scroll to top page */
		}, 800);
		return false;

	});	
/*--------------------------------------------------
 Change section height when window resize
--------------------------------------------------*/
    resizeContent();
	
    $(window).resize(function() {
        //resizeContent();
        if($(window).height() > (lastSectionObj.height()) ){
	        $('.sections:last').height($(window).height())
	      }
	      pageFixed();

    });
	
	function resizeContent() {
		var winHeight = $(window).height();
		var footerHeight = $('#footer').height();
		var lastSectionObj = $('.sections:last');
		var realHeightLastSection = $(lastSectionObj).css('height', 'auto').height();
		
		var heightNAV = $('ul.list-nav').height() + 60;
		var newHeight = winHeight - footerHeight - heightNAV;
		
		if(newHeight >= realHeightLastSection){
			$(lastSectionObj).css('height', newHeight);
		}
		
	}

/*--------------------------------------------------
  $(window).scroll
--------------------------------------------------*/
	// NAV + Page Top effect
	$(window).scroll(function(){
		//
		pageFixed();
		
		//Fixed NAV
		if($(this).scrollTop() > 166 ) {
			$('#nav').addClass("navChange");
		} else {
			$('#nav').removeClass("navChange");
		}
		
		// NAV Scroll
		if (!flag) {
			return;	
		}
		var _top = $('.sections[ref=1]', containerObj).offset().top;
		var top = $(this).scrollTop() + 64;
		
		$('a', tabObj).removeClass('current');
		
		var top1 = _top;
		
		for (var i = 0; i < counter; i++) {
			var j = i + 1;
			top1 += elementsHeight.hasOwnProperty('section-' + i) ? elementsHeight['section-' + i] : 0;
			
			var top2 = top1 + elementsHeight['section-' + j];
			
			if (top + i * 30 >= top1 && top < top2) {
				$('a', tabObj).removeClass('current');
				$('a[rel=' + j + ']', tabObj).addClass('current');
			}
			
		}
		
	});
});


