module.exports = function (grunt) {
  grunt.initConfig({
    compass: {                  // Task
      dist: {                   // Target
        options: {              // Target options
          /*httpPath: '/',          
          sassDir: 'css',
          cssDir: 'css',
          imagesDir: 'images',*/
          config: 'config.rb',
          outputStyle: 'expanded'
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.registerTask('default', [
    'compass'
  ]);
};

//cd /Volumes/Macintosh\ HD/Thainguyen/KGU-demo